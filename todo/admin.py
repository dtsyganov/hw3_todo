from django.contrib import admin

# from django.utils import timezone

from .models import Task


@admin.action(description="Change tasks status")
def change_status(modeladmin, request, queryset):
    #    queryset.update(done=True, done_date = timezone.now())
    for task in queryset:
        task.done = not (task.done)
        task.save()


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    search_fields = ["task_name"]
    readonly_fields = ("create_date", "done_date")
    list_filter = ["done"]
    list_display = ("task_name", "create_date", "done", "done_date")
    actions = [change_status]
