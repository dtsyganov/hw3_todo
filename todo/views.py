from django.views.generic import (
    DetailView,
    CreateView,
    ListView,
    UpdateView,
    DeleteView,
)
from todo.models import Task


class TaskCreateView(CreateView):
    # Представление для создания одной задачи
    model = Task
    fields = ["task_name", "done"]
    template_name = "task_create.html"
    success_url = "/"


class TaskUpdateView(UpdateView):
    # Представление для редактирования одной задачи
    model = Task
    fields = ["task_name", "done"]
    context_object_name = "task"
    template_name = "task_create.html"
    success_url = "/"


class TaskDeleteView(DeleteView):
    # Представление для удаления одной задачи
    model = Task
    template_name = "task_delete.html"
    success_url = "/"


class TaskDetailView(DetailView):
    # Представление для отображения одной задачи
    model = Task
    context_object_name = "task"
    template_name = "task_detail.html"


class TaskListView(ListView):
    # Представление для отображения списка незавершенных задач
    context_object_name = "tasks"
    queryset = Task.objects.filter(done=False)
    template_name = "task_list.html"


class TaskFullListView(ListView):
    # Представление для отображения полного списка задач
    context_object_name = "tasks"
    queryset = Task.objects.all()
    template_name = "task_fulllist.html"
