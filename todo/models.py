from django.db import models
from django.utils import timezone


class Task(models.Model):
    task_name = models.CharField(max_length=200, help_text="Название задачи")
    create_date = models.DateTimeField(auto_now=True, help_text="Дата создания задачи")
    done = models.BooleanField(default=False, help_text="Выполнена ли задача")
    done_date = models.DateTimeField(
        default=None,
        blank=True,
        null=True,
        editable=False,
        help_text="Дата выполнения задачи",
    )

    def save(self, *args, **kwargs) -> None:
        """Переопределение сохранения.
        Ставится время выполнения, если задача выполнена и удаляется время выполнения в обратном случае.
        """
        if self.done:
            self.done_date = timezone.now()
        else:
            self.done_date = None
        return super().save(*args, **kwargs)

    def __str__(self):
        """переопределение строкового представления объекта."""
        return f"Task: {self.task_name}"
